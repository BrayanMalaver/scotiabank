package com.scotiabank.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.scotiabank.dto.Persona;
import com.scotiabank.repository.IRepository;

@RestController
@RequestMapping("/customer")
public class Controller {
	
	 @Autowired
	 IRepository iRepository;
	    
	    @GetMapping()
		public List<Persona> list() {
	        return iRepository.findAll();
	    }
	    
	    @GetMapping("/{id}")
	    public Optional get(@PathVariable String id) {
	        Optional<Persona> Persona = iRepository.findById(Long.parseLong(id));
	        return Persona;
	    }
	 	    
	    @PostMapping
	    public ResponseEntity<?> post(@RequestBody Persona input) {
	        Persona persona = iRepository.save(input);
	        return ResponseEntity.ok(persona);
	    }
	    
	    

}
