package com.scotiabank.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.scotiabank.dto.Persona;

public interface IRepository extends JpaRepository<Persona, Long>{

}
